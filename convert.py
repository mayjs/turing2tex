import sys

def convert(file, output):
	init = None
	accept = []
	transitions = []
	trans_begin = None
	nodes = set()
	#parse input lines
	for line in file:
		line = line.strip()
		if line.startswith("//"):
			pass
		elif line.startswith("init:"):
			init = line[5:].strip()
			nodes.add(init)
		elif line.startswith("accept:"):
			accept = [x.strip() for x in line[7:].split(",")]
			for x in accept:
				nodes.add(x)
		elif trans_begin != None:
			trans = line.split(",")
			(cstate,read,nstate,write,direction) = (trans_begin[0], trans_begin[1], trans[0], trans[1], trans[2])
			transitions.append((cstate,read,nstate,write,direction))
			nodes.add(cstate)
			nodes.add(nstate)
			trans_begin = None
		elif len(line.split(",")) == 2:
			parts = line.split(",")
			trans_begin = (parts[0],parts[1])

	#generate latex code
	output.write("\\begin{tikzpicture}[shorten >= 1pt, node distance=4cm, on grid, auto]\n")
	output.write("\\node[state, initial] (%s) {$q_{%s}$};\n"%(init,init))

	addedNodes = set()
	addedNodes.add(init)

	lastNodeD = {"value":init}

	#add all nodes
	def appendNode(n):
		lastNode = lastNodeD["value"]
		if not n in addedNodes:
			if n in accept:
				output.write("\\node[state, accepting] (%s) [right=of %s] {$q_{%s}$};\n"%(n,lastNode,n))
			else:
				output.write("\\node[state] (%s) [right=of %s] {$q_{%s}$};\n"%(n,lastNode,n))
			addedNodes.add(n)
			lastNodeD["value"] = n

	for (cstate,read,nstate,write,direction) in transitions:
		appendNode(cstate)
		appendNode(nstate)

	#calculate transition labels
	trans_labels = dict()
	texcodes = {">":"\\rightarrow", "<":"\\leftarrow", "-":"\\downarrow", "_":"\\textvisiblespace"}
	for (cstate,read,nstate,write,direction) in transitions:
		if read=="_":
			read = texcodes[read]
		if write=="_":
			write = texcodes[write]
		if not (cstate,nstate) in trans_labels:
			trans_labels[(cstate, nstate)] = []
		trans_labels[(cstate,nstate)].append("%s:%s,%s"%(read,write,texcodes[direction]))
	
	#generate transition code
	output.write("\n\path[->]")
	for ((source,target),labels) in trans_labels.iteritems():
		output.write("\n (%s) edge node {$\\begin{matrix} %s \\end{matrix}$} (%s)"%(source,"\\\\".join(labels),target))
	output.write(";\n\\end{tikzpicture}\n")

with open("turing.txt") as f:
	convert(f, sys.stdout)