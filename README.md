# Turing machine to LaTeX converter
This python script converts the syntax from https://turingmachinesimulator.com/ to LaTeX code.  
It will NOT position the nodes for you or bend edges in any way, it only creates the needed nodes and edges that you can position afterwards.

To use, you need the following at the top of your document:  
```latex
\usepackage{tikz, wasysym}
\usetikzlibrary{automata,positioning}
\tikzset{initial text={}}

\newsavebox{\spacebox}
\begin{lrbox}{\spacebox}
\verb*! !
\end{lrbox}
\newcommand{\aspace}{\usebox{\spacebox}}

\def\tublank{\aspace}
```

If you want another blank character you can adjust the `\tublank` macro.

Currently loading from a file given as command line argument is not supported. It will instead always try to load turing.txt
